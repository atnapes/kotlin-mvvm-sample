package com.atnapes.voemotestjob

import android.content.Context
import com.atnapes.voemotestjob.di.DaggerAppComponent
import com.atnapes.voemotestjob.di.NetworkModule
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class App : DaggerApplication() {

    lateinit var androidInjector: AndroidInjector<out DaggerApplication>

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)

        androidInjector = DaggerAppComponent.builder()
            .network(networkModule())
            .application(this)
            .build()
    }

    public override fun applicationInjector(): AndroidInjector<out DaggerApplication> = androidInjector

    protected open fun networkModule(): NetworkModule = NetworkModule()

}