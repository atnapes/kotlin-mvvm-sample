package com.atnapes.voemotestjob.di

import com.atnapes.voemotestjob.data.repository.FoodRepository
import com.atnapes.voemotestjob.data.repository.FoodRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {

    @Binds
    fun bindFoodRepository(repository: FoodRepositoryImpl): FoodRepository
}