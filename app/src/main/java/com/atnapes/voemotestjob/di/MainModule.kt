package com.atnapes.voemotestjob.di

import com.atnapes.voemotestjob.ui.restauransts.RestaurantFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class MainModule {
    @ContributesAndroidInjector
    internal abstract fun contributeTopFragmentInjector(): RestaurantFragment
}