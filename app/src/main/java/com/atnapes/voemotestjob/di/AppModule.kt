package com.atnapes.voemotestjob.di

import android.app.Application
import com.atnapes.voemotestjob.App
import dagger.Binds
import dagger.Module


@Module(includes = [NetworkModule::class, ViewModelModule::class])
abstract class AppModule {

    @Binds
    internal abstract fun application(app: App): Application

}
