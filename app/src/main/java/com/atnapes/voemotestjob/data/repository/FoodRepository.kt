package com.atnapes.voemotestjob.data.repository

import com.atnapes.voemotestjob.data.local.Food
import io.reactivex.Flowable

interface FoodRepository {

    fun getFood(): Flowable<Food>
}
