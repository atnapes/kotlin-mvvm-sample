package com.atnapes.voemotestjob.data.repository

import com.atnapes.voemotestjob.data.local.Food
import com.atnapes.voemotestjob.data.local.FoodDataSource
import io.reactivex.Flowable
import javax.inject.Inject

class FoodRepositoryImpl @Inject constructor(
    private val foodDataSource: FoodDataSource
) : FoodRepository {

    override fun getFood(): Flowable<Food> {

        return foodDataSource.getFood()
    }
}