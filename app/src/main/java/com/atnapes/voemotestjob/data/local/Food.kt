package com.atnapes.voemotestjob.data.local

import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class Food (
    val id: String? = null,
    val title: String? = null,
    val subTitle: String? = null,
    val image: String? = null,
    val preparation: Int? = null,
    val cuisineType: BaseType? = null,
    val mealType: BaseType? = null
)

@JsonSerializable
data class BaseType(
    val title: String
)
