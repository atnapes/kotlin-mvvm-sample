package com.atnapes.voemotestjob.data.local

import com.atnapes.voemotestjob.App
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types.newParameterizedType
import io.reactivex.Flowable
import javax.inject.Inject

class FoodDataSource @Inject constructor(val app: App, val moshi: Moshi) {

    fun getFood(): Flowable<Food> {
        val json = app.assets.open("data.json")
            .bufferedReader().use { it.readText() }

        val listMyData = newParameterizedType(List::class.java, Food::class.java)
        val transactionJsonAdapter : JsonAdapter<List<Food>> = moshi.adapter(listMyData)
        val foods = transactionJsonAdapter.fromJson(json)
        return Flowable.fromIterable(foods)
    }
}
