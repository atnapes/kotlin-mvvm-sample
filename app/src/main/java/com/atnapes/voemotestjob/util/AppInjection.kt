package com.atnapes.voemotestjob.util

import android.content.Context
import com.atnapes.voemotestjob.App
import com.atnapes.voemotestjob.di.AppComponent

object AppInjection {
    fun of(context: Context?): AppComponent {
        return (context as App).applicationInjector() as AppComponent
    }
}