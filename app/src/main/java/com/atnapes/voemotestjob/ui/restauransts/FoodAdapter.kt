package com.atnapes.voemotestjob.ui.restauransts

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.HORIZONTAL
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import com.atnapes.voemotestjob.R
import com.atnapes.voemotestjob.data.local.Food
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find

// So this adapter shows breakfasts in a horizontal and other type of foods in a vertical list
class FoodAdapter: ListAdapter<Food, RecyclerView.ViewHolder>(DiffCallback) {

    companion object {
        private const val VIEW_TYPE_HORIZONTAL_LIST = 0
        private const val VIEW_TYPE_ITEM = 1
    }

    private var breakfastList: List<Food> = emptyList()
    private var nonBreakfastList: List<Food> = emptyList()

    private val dummyFirstItem = Food()
    private val isBreakfastPredicate =
        { food: Food -> TextUtils.equals(food.mealType?.title, "Breakfast") }
    private val horizontalAdapter = HorizontalAdapter()

    override fun getItemViewType(position: Int) =
        if (position == 0 && hasBreakfastItems()) VIEW_TYPE_HORIZONTAL_LIST else VIEW_TYPE_ITEM

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_HORIZONTAL_LIST) {
            val itemView =
                HorizontalRecyclerViewViewHolderUI().createView(AnkoContext.create(parent.context, parent))
            return HorizontalViewHolder(itemView)
        }
        return FoodViewHolder.newInstance(parent, false)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == 0 && hasBreakfastItems()) {
            val hRecyclerView = holder.itemView.find<RecyclerView>(R.id.recyclerview_main_horizontal)
            hRecyclerView.layoutManager =
                LinearLayoutManager(hRecyclerView.context, HORIZONTAL, false)
            hRecyclerView.adapter = horizontalAdapter
        } else {
            val foodViewHolder = holder as FoodViewHolder
            foodViewHolder.bindTo(getItem(position))
        }
    }

    private fun hasBreakfastItems() = breakfastList.isNotEmpty()

    override fun submitList(list: List<Food>?) {
        if (null == list || list.isEmpty()) {
            super.submitList(list)
        } else {
            val (list1, list2) = list.partition(isBreakfastPredicate)
            breakfastList = list1
            val mutableList = mutableListOf<Food>()
            if (breakfastList.isNotEmpty()) {
                mutableList.add(dummyFirstItem)
            }
            mutableList.addAll(list2)
            nonBreakfastList = mutableList
            super.submitList(nonBreakfastList)
            horizontalAdapter.submitList(breakfastList)
        }
    }

    object DiffCallback : DiffUtil.ItemCallback<Food>() {
        override fun areItemsTheSame(oldItem: Food?, newItem: Food?) = oldItem?.id == newItem?.id
        override fun areContentsTheSame(oldItem: Food?, newItem: Food?) = oldItem?.id == newItem?.id
    }

    class HorizontalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class HorizontalAdapter: ListAdapter<Food, FoodViewHolder>(DiffCallback) {
        override fun onBindViewHolder(holder: FoodViewHolder, position: Int) =
            holder.bindTo(getItem(position))

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder =
            FoodViewHolder.newInstance(parent, true)
    }
}