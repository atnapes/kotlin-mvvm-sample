package com.atnapes.voemotestjob.ui.restauransts

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.atnapes.voemotestjob.R
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

class RestaurantFragmentUI : AnkoComponent<RestaurantFragment> {

    override fun createView(ui: AnkoContext<RestaurantFragment>): View = with(ui) {

        recyclerView {
            id = R.id.recyclerview_main
            verticalPadding = dip(8)
            horizontalPadding = dip(12)
            clipToPadding = false
            clipChildren = false
            clipToOutline = false
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = FoodAdapter()
        }

    }
}
