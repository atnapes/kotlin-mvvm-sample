package com.atnapes.voemotestjob.ui.restauransts

import android.support.constraint.ConstraintSet.PARENT_ID
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.atnapes.voemotestjob.R
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint

class VerticalFoodMenuItemUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            frameLayout {
                cardView {
                    radius = dip(4).toFloat()

                    constraintLayout {

                        val hMargin = dip(12)

                        val imageViewId = R.id.imageview_horizontalfooditem_image
                        val textTitleId = R.id.textview_horizontalfooditem_title
                        val textTypeId = R.id.textview_horizontalfooditem_type
                        val textDurationId = R.id.textview_horizontalfooditem_duration
                        val ratingBarId = R.id.ratingbar_horizontalfooditem_rating

                        imageView {
                            id = imageViewId
                            scaleType = ImageView.ScaleType.CENTER_CROP
                        }.lparams(dip(160), matchConstraint) {
                            topToTop = PARENT_ID
                            bottomToBottom = PARENT_ID
                            leftToLeft = PARENT_ID
                        }

                        themedTextView(R.style.TextTitle){
                            id = textTitleId
                        }.lparams {
                            topToTop = PARENT_ID
                            leftToRight = imageViewId
                            horizontalMargin = hMargin
                            topMargin = dip(8)
                        }

                        themedTextView(R.style.Text) {
                            id = textTypeId
                        }.lparams {
                            topToBottom = textTitleId
                            leftToLeft = textTitleId
                            topMargin = dip(2)
                        }

                        themedTextView(R.style.Text) {
                            id = textDurationId
                        }.lparams {
                            leftToLeft = textTitleId
                            bottomToBottom = PARENT_ID
                            bottomMargin = dip(8)
                        }

                        themedRatingBar(R.style.RatingBar) {
                            id = ratingBarId
                            scaleY = .5f
                            scaleX = .5f
                            translationX = dip(60).toFloat()
                        }.lparams {
                            rightToRight = PARENT_ID
                            bottomToBottom = PARENT_ID
                            rightMargin = dip(8)
                        }
                    }.lparams(matchParent, matchParent)

                }.lparams(matchParent, height = dip(150)) {
                    verticalMargin = dip(8)
                }
            }
        }
    }
}