package com.atnapes.voemotestjob.ui.restauransts

import android.support.constraint.ConstraintSet.PARENT_ID
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.atnapes.voemotestjob.R
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint


class HorizontalFoodMenuItemUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            frameLayout {
                clipChildren = false
                cardView {
                    radius = dip(6).toFloat()

                    constraintLayout {

                        val hMargin = dip(12)

                        val imageViewId = R.id.imageview_horizontalfooditem_image
                        val textTitleId = R.id.textview_horizontalfooditem_title
                        val textTypeId = R.id.textview_horizontalfooditem_type
                        val textDurationId = R.id.textview_horizontalfooditem_duration
                        val textExtraId = R.id.textview_horizontalfooditem_extra
                        val ratingBarId = R.id.ratingbar_horizontalfooditem_rating

                        imageView {
                            id = imageViewId
                            scaleType = ImageView.ScaleType.CENTER_CROP
                        }.lparams(matchConstraint, dip(140)) {
                            topToTop = PARENT_ID
                            leftToLeft = PARENT_ID
                            rightToRight = PARENT_ID
                        }

                        textView(context.getString(R.string.restaurantsfragment_fooditem_fab)) {
                            ellipsize = TextUtils.TruncateAt.END
                            padding = dip(6)
                            textSize = 22f
                            textColorResource = R.color.color_textOnPrimary
                            backgroundResource = R.drawable.horizontalfooditem_fab_background
                        }.lparams(dip(64), dip(64)) {
                            topToBottom = imageViewId
                            bottomToBottom = imageViewId
                            rightToRight = imageViewId
                            rightMargin = dip(16)
                        }

                        themedTextView(R.style.TextTitle){
                            id = textTitleId
                        }.lparams {
                            topToBottom = imageViewId
                            leftToLeft = imageViewId
                            horizontalMargin = hMargin
                            topMargin = dip(8)
                        }

                        themedTextView(R.style.Text) {
                            id = textTypeId
                        }.lparams {
                            leftToLeft = textTitleId
                            topToBottom = textTitleId
                            topMargin = dip(2)
                        }

                        themedTextView(R.style.Text) {
                            id = textDurationId
                        }.lparams {
                            leftToRight = textTypeId
                            baselineToBaseline = textTypeId
                            marginStart = hMargin
                        }

                        themedTextView("No min", R.style.TextGreen) {
                            id = textExtraId
                        }.lparams {
                            leftToRight = textDurationId
                            baselineToBaseline = textTypeId
                            marginStart = hMargin
                        }

                        themedRatingBar(R.style.RatingBar) {
                            id = ratingBarId
                            scaleY = .5f
                            scaleX = .5f
                            translationX = dip(-60).toFloat()
                        }.lparams {
                            leftToLeft = textTitleId
                            topToBottom = textTypeId
                            topMargin = dip(2)
                        }

                        textView(context.getString(R.string.restaurantsfragment_fooditem_firstorderdesc)) {
                            backgroundColorResource = R.color.color_divider
                            gravity = Gravity.START
                            topPadding = dip(8)
                            horizontalPadding = dip(12)
                        }.lparams(matchParent, dip(40)) {
                            bottomToBottom = PARENT_ID
                            startToStart = PARENT_ID
                            endToEnd = PARENT_ID
                        }

                    }.lparams(matchParent, matchParent)

                }.lparams(width = dip(380), height = dip(300)) {
                    rightPadding = dip(16)
                    margin = dip(2)
                }
            }
        }
    }

}