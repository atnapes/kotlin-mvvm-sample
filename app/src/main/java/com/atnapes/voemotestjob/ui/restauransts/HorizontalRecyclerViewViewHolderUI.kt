package com.atnapes.voemotestjob.ui.restauransts

import android.view.View
import android.view.ViewGroup
import com.atnapes.voemotestjob.R
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

class HorizontalRecyclerViewViewHolderUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            frameLayout {
                clipChildren = false
                clipToOutline = false
                recyclerView {
                    id = R.id.recyclerview_main_horizontal
                    clipToOutline = false
                    clipChildren = false
                }.lparams(matchParent, wrapContent)
            }
        }
    }
}
