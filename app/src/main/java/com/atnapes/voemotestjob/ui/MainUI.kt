package com.atnapes.voemotestjob.ui

import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.View
import org.jetbrains.anko.*
import com.atnapes.voemotestjob.R
import com.atnapes.voemotestjob.ui.MainActivity
import org.jetbrains.anko.design.bottomNavigationView
import org.jetbrains.anko.sdk25.coroutines.onQueryTextListener


class MainUI : AnkoComponent<MainActivity>{

    override fun createView(ui: AnkoContext<MainActivity>): View = with(ui){
        frameLayout {
            verticalLayout {
                id = R.id.verticallayout_main_toolbar
                backgroundColorResource = R.color.toolbar_backgroundcolor
                elevation = dip(4).toFloat()

                themedTextView(context.getString(R.string.app_name), R.style.ToolbarTitle) {
                }.lparams {
                    topMargin = dip(16)
                    bottomMargin = dip(8)
                    gravity = Gravity.CENTER_HORIZONTAL
                }

                linearLayout {
                    gravity = Gravity.CENTER_VERTICAL
                    id = R.id.linearlayout_toolbar_items
                    val searchView = searchView {
                        id = R.id.searchview_toolbar
                        backgroundResource = R.drawable.main_searchview_background
                        queryHint = context.getString(R.string.mainactivity_searchview_hint)
                    }.lparams(width = dip(0)) {
                        weight = 1f
                        margin = dip(8)
                    }
                    searchView.onActionViewExpanded()
                    if (!searchView.isFocused) searchView.clearFocus()
                    searchView.onQueryTextListener {
                        onQueryTextSubmit { false }
                        onQueryTextChange { false }
                    }

                    themedTextView(context.getString(R.string.mainactivity_toolbaricon_cuisine), R.style.ToolbarIcons) {
                        val icon = R.drawable.ic_restaurant_24dp
                        setCompoundDrawablesWithIntrinsicBounds(0, icon, 0, 0)
                    }.lparams(width = dimen(R.dimen.height_toolbaritems))
                    themedTextView(context.getString(R.string.mainactivity_toolbaricon_refine), R.style.ToolbarIcons) {
                        val icon = R.drawable.ic_refine_24dp
                        setCompoundDrawablesWithIntrinsicBounds(0, icon, 0, 0)
                    }.lparams(width = dimen(R.dimen.height_toolbaritems))

                }.lparams(width = matchParent, height = dimen(R.dimen.height_toolbaritems)) {
                    gravity = top
                }
            }.lparams(width = matchParent, height = dip(104))

            frameLayout{
                id = R.id.framelayout_mainactivity_container
                backgroundColorResource = R.color.colorBackground
            }.lparams(width = matchParent, height = matchParent) {
                gravity = Gravity.CENTER
                topMargin = dip(96)
                bottomMargin = dip(80)
            }

            bottomNavigationView {
                backgroundColorResource = R.color.bottomnav_backgroundcolor
                id = R.id.bottomnavigation_mainactivity_navbar
                val tint = ContextCompat.getColorStateList(context, R.color.bottomnav_colorstatelist)
                itemIconTintList = tint
                itemTextColor = tint
                inflateMenu(R.menu.menu_navbar)

                view {
                    backgroundColorResource = R.color.color_divider
                }.lparams(width = matchParent, height = dip(.5f)) {
                    gravity = Gravity.TOP
                }

            }.lparams(height = dimen(R.dimen.height_mainactivity_navbar), width = matchParent) {
                gravity = Gravity.BOTTOM
            }

        }
    }
}
