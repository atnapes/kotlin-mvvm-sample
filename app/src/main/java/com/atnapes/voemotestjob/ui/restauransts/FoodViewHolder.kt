package com.atnapes.voemotestjob.ui.restauransts

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.atnapes.voemotestjob.R
import com.atnapes.voemotestjob.data.local.Food
import com.bumptech.glide.Glide
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import java.util.*

class FoodViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

    companion object {
        fun newInstance(parent: ViewGroup, isHorizontal: Boolean) : FoodViewHolder {
            val ui: AnkoComponent<ViewGroup>
            if (isHorizontal) {
                ui = HorizontalFoodMenuItemUI()
            } else {
                ui = VerticalFoodMenuItemUI()
            }
            val itemView = ui.createView(AnkoContext.create(parent.context, parent))
            if (!isHorizontal) {
                itemView.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            }
            return FoodViewHolder(itemView)
        }
    }

    private val imageView = itemView.find<ImageView>(R.id.imageview_horizontalfooditem_image)
    private val textViewTitle = itemView.find<TextView>(R.id.textview_horizontalfooditem_title)
    private val textViewType = itemView.find<TextView>(R.id.textview_horizontalfooditem_type)
    private val textViewDuration = itemView.find<TextView>(R.id.textview_horizontalfooditem_duration)
    private val ratingBar = itemView.find<RatingBar>(R.id.ratingbar_horizontalfooditem_rating)

    fun bindTo(item: Food?) {
        Glide.with(itemView).load(item?.image).into(imageView)

        textViewTitle.text = item?.title

        textViewType.text = String.format(Locale.getDefault(),
            "%s, %s", item?.cuisineType?.title, item?.mealType?.title)

        textViewDuration.text = itemView.context.getString(
            R.string.restaurantsfragment_fooditem_duration_format, item?.preparation, item?.preparation?.plus(5)
        )

        ratingBar.setIsIndicator(true)
        ratingBar.rating = 4f
    }

}
