package com.atnapes.voemotestjob.ui.restauransts

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.atnapes.voemotestjob.R
import dagger.android.support.AndroidSupportInjection
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import javax.inject.Inject

class RestaurantFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: FoodViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(FoodViewModel::class.java)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    var query : String? = ""
        set(value) {
            field = value
            if (null == value) viewModel.query = ""
            else viewModel.query = value
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return RestaurantFragmentUI()
            .createView(AnkoContext.create(context!!, this))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = view.find<RecyclerView>(R.id.recyclerview_main).adapter as FoodAdapter

        viewModel.mediatorLiveData.observe(this, Observer { adapter.submitList(it) })
    }

    companion object {
        fun newInstance() : RestaurantFragment = RestaurantFragment()
    }
}