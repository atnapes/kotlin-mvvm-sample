package com.atnapes.voemotestjob.ui.restauransts

import android.arch.lifecycle.*
import android.util.Log
import com.atnapes.voemotestjob.data.local.Food
import com.atnapes.voemotestjob.data.repository.FoodRepository
import com.atnapes.voemotestjob.util.ext.toLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.Flowable
import javax.inject.Inject

class FoodViewModel @Inject constructor(
    private val foodRepository: FoodRepository
) : ViewModel(), LifecycleObserver {

    var query: CharSequence = ""
    set(value) {
        field = value
        refresh()
    }

    val mediatorLiveData = MediatorLiveData<List<Food>>()

    private var restaurant: LiveData<List<Food>> = createLiveData()

    private fun createLiveData(): LiveData<List<Food>> {
        return foodRepository.getFood()
            .filter {
                if (null == it.title) return@filter false
                else it.title.toLowerCase().contains(query.toString().toLowerCase())
            }
            .toList()
            .toFlowable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError(defaultErrorHandler())
            .toLiveData()
    }

    init {
        mediatorLiveData.addSource(restaurant) { mediatorLiveData.value = it }
    }

    fun refresh() {
        mediatorLiveData.removeSource(restaurant)
        restaurant = createLiveData()
        mediatorLiveData.addSource(restaurant) { mediatorLiveData.value = it }
    }
}


fun defaultErrorHandler(): (Throwable) -> Unit = { e -> Log.e("FoodViewModel", e.message, e) }