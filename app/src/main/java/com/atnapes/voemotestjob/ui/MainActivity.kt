package com.atnapes.voemotestjob.ui

import android.os.Bundle
import android.widget.SearchView.OnQueryTextListener
import android.view.View
import android.widget.SearchView
import com.atnapes.voemotestjob.R
import com.atnapes.voemotestjob.ui.restauransts.RestaurantFragment
import dagger.android.support.DaggerAppCompatActivity
import org.jetbrains.anko.find
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.toast

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MainUI().setContentView(this)

        val fragment = RestaurantFragment.newInstance()
        supportFragmentManager
            .beginTransaction()
            .add(R.id.framelayout_mainactivity_container, fragment)
            .commit()

        class MyQueryListener :OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                fragment.query = p0
                return true
            }
            override fun onQueryTextChange(p0: String?): Boolean {
                fragment.query = p0
                return true
            }
        }
        find<SearchView>(R.id.searchview_toolbar).setOnQueryTextListener(MyQueryListener())

        find<View>(R.id.navtab_main_second).setOnClickListener { toast("Not Implemented!") }
        find<View>(R.id.navtab_main_third).setOnClickListener { toast("Not Implemented!") }
    }

}
